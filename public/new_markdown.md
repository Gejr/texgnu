<h1 align="center">
  <br>
    <a href="https://gitlab.com/Gejr/TeXGNU"><img src="logo.svg" alt="TeXGNU" width="200"></a>
  <br>
  TeXGNU
  <br><br>
</h1>

[TeXGNU](https://gitlab.com/Gejr/TeXGNU) is a cross-platform Markdown book built with [Electron](https://electron.atom.io/) and [React](https://facebook.github.io/react/)

## Syntax Highlighting

TeXGNU supports a variety of (code) languages.

### JavaScript:

```javascript
function foo() {
  return "Hello, TeXGNU!"
}
```

## The Features of TeXGNU

The team behind TeXGNU have many exciting future plans for the editor.

TeXGNU currently offers a markdown editor with a live preview on the right.

## Roadmap

A big feature TeXGNU have planned is <img src="logo.svg" alt="TeXGNU" width="20"> support with live-previewing.

---

## Development

```
yarn install
yarn run electron-dev
```

## Build package

```
yarn install
yarn run electron-pack
```

## How to contribute

Contribute with either emailing me at [texgnu@gejr.dk](mailto:texgnu@gejr.dk) or by submitting a pull request at [the gitlab repo](https://gitlab.com/Gejr/TeXGNU).

You can contribute with anything you want. Any help is appreciated.


### Credits

TeXGNU is built upon many diffrent open source packages. __Massive thank you and credit__ to the authors of these packages! ♥♥
