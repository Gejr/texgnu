import React, { Component } from 'react';
import Editor from './editor.js';
import SplitPane from 'react-split-pane';
import ReactMarkdown from 'react-markdown';
import logo from './logo.svg';
import './App.css';

const fs = window.require('fs');
const isDev = window.require('electron-is-dev');
const path = window.require('path');

class App extends Component {
	constructor(props) {
  		super();

		// Open the readme file
	    const file_name = path.join((isDev ? '' : window.process.resourcesPath), "public/new_markdown.md");
	    const content = fs.readFileSync(file_name).toString();

  		this.state = {
    		source: content
  		}
		this.onSourceChange = this.onSourceChange.bind(this);
	}
	onSourceChange(src) {
	  this.setState({
	    source: src
	  });
	}

  	render() {
    	return (
      		<div className="App">
        		<SplitPane split="vertical" defaultSize="50%">
					<div className="editor-pane">
						<Editor className="editor" value={this.state.source} onChange={this.onSourceChange}/>
					</div>
					<div className="view-pane">
						<ReactMarkdown className="result" source={this.state.source} />
					</div>
				</SplitPane>
      		</div>
    	);
  	}
}

export default App;
